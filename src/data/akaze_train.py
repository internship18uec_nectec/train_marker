import cv2
import numpy as np
from src.models.alternative_random_forests import AlternatingDecisionForests
from src.features.affine_base import affine_detect

def run():
    img_path="/home/cde/.ghq/bitbucket.org/internship18uec_nectec/train_marker/data/raw/marker_mugcup.png"
    mask_path="/home/cde/.ghq/bitbucket.org/internship18uec_nectec/train_marker/data/raw/mask_mugcup.png"

    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

    detector = cv2.AKAZE_create()
    base_kp, base_dsc = detector.detectAndCompute(gray, mask)
    keypoints, descriptions = affine_detect(detector, gray, mask)


