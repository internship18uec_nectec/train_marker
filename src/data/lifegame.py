#!/home/cde/.ghq/bitbucket.org/internship18uec_nectec/train_marker/.venv/bin/python
import copy
import matplotlib.pyplot as plt

HEIGHT = 50
WIDTH = 50
EPOCH = 100

# 現在の世代の環境
env_grid = [[0 for i in range(WIDTH)] for j in range(HEIGHT)]

# 銀河を真ん中に置く
c = 30
for i in range(6):
    env_grid[c+0][c+i] = 1
    env_grid[c+1][c+i] = 1
    
    env_grid[c+i][c+7] = 1
    env_grid[c+i][c+8] = 1

    env_grid[c+i+3][c+0] = 1
    env_grid[c+i+3][c+1] = 1

    env_grid[c+7][c+i+3] = 1
    env_grid[c+8][c+i+3] = 1

# 表示の準備
fig, ax = plt.subplots(1,1, figsize=(8,8))
imgs = ax.imshow(env_grid, cmap="GnBu")

for epoch in range(EPOCH):
    # 次の世代の環境を定義
    new_grid = [[0 for i in range(WIDTH)] for j in range(HEIGHT)] 
    for j in range(1, HEIGHT-1):
        for i in range(1,WIDTH-1):
            center_x = i
            center_y = j
            # 周囲の生存数を数える
            count = 0
            count += env_grid[center_y-1][center_x-1]
            count += env_grid[center_y-1][center_x]
            count += env_grid[center_y-1][center_x+1]
            count += env_grid[center_y][center_x-1]
            count += env_grid[center_y][center_x+1]
            count += env_grid[center_y+1][center_x-1]
            count += env_grid[center_y+1][center_x]
            count += env_grid[center_y+1][center_x+1]

            # 誕生・生存・過疎・過密でセルを更新する
            if (count == 3) and (env_grid[center_y][center_x]==0):
                new_grid[center_y][center_x] = 1
            elif ((count == 3) or (count == 2)) and (env_grid[center_y][center_x]==1):
                new_grid[center_y][center_x] = 1
            elif (count <= 1) and (env_grid[center_y][center_x]==1):
                new_grid[center_y][center_x] = 0
            elif (count >= 4) and (env_grid[center_y][center_x]==1):
                new_grid[center_y][center_x] = 0
    env_grid = copy.copy(new_grid)
    # 表示する
    imgs.set_data(env_grid)
    plt.pause(.01)
