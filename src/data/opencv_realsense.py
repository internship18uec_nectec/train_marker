#!/home/cde/.ghq/bitbucket.org/internship18uec_nectec/train_marker/.venv/bin/python
import pyrealsense2 as rs
import numpy as np
import cv2
from matplotlib import pyplot as plt

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipeline.start(config)

try:
    while True:

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()
        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())

        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image,
                                                               alpha=0.03),
                                           cv2.COLORMAP_JET)

        # Stack both images horizontally
        images = np.hstack((color_image, depth_colormap))

        # Show images
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', images)
        COLOR = ('b', 'g', 'r')
        plt.clf()
        for idx, c in enumerate(COLOR):
            hist, bins = np.histogram(color_image[:, :, idx].ravel(), 256, [0, 256])
            plt.plot(hist, color=c)
            plt.xlim([0, 256])
        plt.pause(0.01)
        key = cv2.waitKey(1) & 0xff
        if key == ord('q'):
            cv2.destroyAllWindows()
            plt.close()
            exit()

finally:
    # Stop streaming
    pipeline.stop()
