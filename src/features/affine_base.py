#!/.venv python3

import cv2
import numpy as np

# %%
def affine_skew(tilt, phi, img, mask=None):
    """
    affine_skew(tilt, phi, img, mask=None) calculates skew_img, skew_mask, affine_inverse
    affine_inverse - is an affine transform matrix from skew_img to img
    """
    h, w = img.shape[:2]
    if mask is None:
        mask = np.zeros((h, w), np.uint8)
        mask[:] = 255
    affine = np.float32([[1, 0, 0], [0, 1, 0]])
    if phi != 0.0:
        phi = np.deg2rad(phi)
        s, c = np.sin(phi), np.cos(phi)
        affine = np.float32([[c, -s], [s, c]])
        corners = [[0, 0], [w, 0], [w, h], [0, h]]
        tcorners = np.int32(np.dot(corners, affine.T))
        x, y, w, h = cv2.boundingRect(np.reshape(tcorners, (1, -1, 2)))
        affine = np.hstack([affine, [[-x], [-y]]])
        img = cv2.warpAffine(
            img, affine, (w, h), flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REPLICATE)
    if tilt != 1.0:
        s = 0.8*np.sqrt(tilt*tilt-1)
        img = cv2.GaussianBlur(img, (0, 0), sigmaX=s, sigmaY=0.01)
        img = cv2.resize(img, (0, 0), fx=1.0/tilt, fy=1.0,
                         interpolation=cv2.INTER_NEAREST)
        affine[0] /= tilt
    if phi != 0.0 or tilt != 1.0:
        h, w = img.shape[:2]
        mask = cv2.warpAffine(mask, affine, (w, h), flags=cv2.INTER_NEAREST)
    aff_inverse = cv2.invertAffineTransform(affine)
    return img, mask, aff_inverse


def affine_detect(detector, img, mask=None):
    params = [(t, phi) for phi in np.reciprocal(np.cos(np.radians(np.arange(5, max_p, offset_p))))
              for t in np.reciprocal(np.cos(np.radians(np.arange(0, max_t, offset_t))))]
    print(len(params))

    if len(params) == 1:
        keypoints, descrs = detector.detectAndCompute(img, mask)
        return keypoints, np.array(descrs)

    def f(param):
        tilt, phi = param
        timg, tmask, Ai = affine_skew(tilt, phi, img, mask)
        keypoints, descrs = detector.detectAndCompute(timg, tmask)
        for kp in keypoints:
            x, y = kp.pt
            kp.pt = tuple(np.dot(Ai, (x, y, 1)))
        if descrs is None:
            descrs = []
        return keypoints, descrs

    keypoints, descrs = [], []
    ires = list(map(f, params))

    for i, (k, desc) in enumerate(ires):
        print(
            'affine sampling: {0:d} / {1:d}\r'.format(i+1, len(params)), end='')
        keypoints.extend(k)
        descrs.extend(desc)

    return keypoints, np.array(descrs)

# %%
# testcase = {"max_t" : 90,
#         "offset_t" : 10,
#         "max_p" : 180,
#         "offset_p" : 10,
# }
max_t = 90
offset_t = 5
max_p = 360
offset_p = 5
# %%
# params = [(t, phi) for phi in np.reciprocal(np.cos(np.radians(np.arange(10, max_t, offset_t)))) for t in np.reciprocal(np.cos(np.radians(np.arange(10, max_t, offset_t))))]

# for t in np.reciprocal(np.cos(np.radians(np.arange(10, max_t, offset_t)))):
#   for phi in np.arange(0, max_p, offset_p):
#     img, mask, ai = affine_skew(t, phi, hoge)
