from time import time
from sklearn import datasets
import pandas as pd
from sklearn.model_selection import train_test_split
from models.alternative_random_forests import AlternatingDecisionForests
from sklearn.ensemble import RandomForestClassifier

iris = datasets.load_iris()
df = pd.DataFrame(iris.data, columns=iris.feature_names)

# 必要ならシャープを外してデータを確認
#df.head()

# separate train data and test data
X = df.loc[:,iris.feature_names].values
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)

adfs = AlternatingDecisionForests(10, 10)

start = time()
adfs.fit(X_train, y_train)
end = time()
ac = adfs.score(X_test, y_test)
time = end-start
print("Alternating Random Forests")
print("accuracy: ", ac)
print("fitting took {} secs".format(time))

rfclf = RandomForestClassifier(n_estimators=10, max_depth=10)
rfstart = time()
rfclf.fit(X_train, y_train)
rfend = time()
rfac = rfclf.score(X_test, y_test)
rftime = rfend-rfstart

print("Random Forests")
print("accuracy: ", rfac)
print("fitting took {} secs".format(rftime))

print("Compair")
print("accuracy ratio: ", )
print("fitting took {} secs".format(end-start))
