import numpy as np
from numba import jit
from tqdm import tqdm


# define image feature value and chose sample by boot strap sampling for each tree
@jit('Tuple((b1[:,:], b1[:,:]))(i8, i8, i8)')
def _bootstrap_sample(n_samples, n_estimators, n_features):
    n_features_forest = int(np.floor(np.sqrt(n_features)))
    features = np.random.choice(n_features, size=(n_estimators, n_features_forest))
    indexes = np.random.randint(2, size=(n_estimators, n_samples)).astype(np.bool)
    columns = np.zeros((n_estimators, n_features)).astype(np.bool)

    for i in range(n_estimators):
        col = np.zeros(n_features).astype(np.bool)
        col[features[i]] = True
        columns[i] = col

    return indexes, columns

@jit('f8(f8[:], i8[:], b1[:], i8[:])', nopython=True)
def _gini_index(w, label, index, classes):
    w = w[index]
    ratio = np.array([w[label==c].sum() / w.sum() for c in classes])

    return (ratio * (1 - ratio)).sum()

@jit('f8(f8[:], i8[:], b1[:], i8[:])', nopython=True)
def _cross_entropy(w, label, index, classes):
    w = w[index]
    ratio = np.array([w[label==c].sum() / w.sum() for c in classes])

    return (ratio[ratio!=0] * np.log(ratio[ratio!=0])).sum()

@jit('f8(f8[:], i8[:], i8[:], i8[:], b1[:], b1[:], b1)', nopython=True)
def _calculate_I(w, classes, y_left, y_right, index_left, index_right, use_entropy=True):
    n_left = len(y_left)
    n_right = len(y_right)
    n_total = n_left + n_right

    if use_entropy:
        left_gain = n_left / n_total * _cross_entropy(w, y_left, index_left, classes)
        right_gain = n_right / n_total * _cross_entropy(w, y_right, index_right, classes)

    else:
        left_gain = n_left / n_total * _gini_index(w, y_left, index_left, classes)
        right_gain = n_right / n_total * _gini_index(w, y_right, index_right, classes)

    return right_gain + left_gain

@jit('Tuple((i8, f8))(f8[:,:], i8[:], f8[:], b1[:], b1[:], i8[:])', nopython=True)
def _split(X, y, w, index, feature_ids, classes):
    d = np.inf
    threshold = 0
    feature_id = 0

    for fi in np.arange(len(feature_ids))[feature_ids]:
        xis = X[:, fi]
        unique = np.unique(xis[index])
        thresholds = (unique[1:] + unique[:-1]) / 2

        for th in thresholds:
            index_left = np.logical_and(index, xis<th)
            index_right = np.logical_and(index, xis>=th)
            y_left = y[index_left]
            y_right = y[index_right]

            I = _calculate_I(w, classes, y_left, y_right, index_left, index_right, True)

            if I < d:
                d = I
                threshold = th
                feature_id = fi

    return feature_id, threshold

@jit('Tuple((i8[:], b1[:], i8[:], b1[:]))(f8[:,:], i8[:], b1[:], i8[:], i8, f8)')
def to_left_and_right(X, y, index, classes, feature_id, threshold):
    index_left = np.logical_and(X[:,feature_id] < threshold, index)
    index_right = np.logical_and(X[:,feature_id] >= threshold, index)
    classes_left, _left = np.unique(y[index_left], return_counts=True)
    classes_right, _right = np.unique(y[index_right], return_counts=True)

    count_left = np.zeros(len(classes))
    count_right = np.zeros(len(classes))

    for i,c in enumerate(classes):
        count_left[i] = _left[classes_left==c].sum()
        count_right[i] = _right[classes_right==c].sum()

    return count_left, index_left, count_right, index_right

def hinge(y):
    norm = np.linalg.norm(y, axis=1)
    return norm / norm.sum()

def exponential(y, margin):
    norm = np.linalg.norm(-y*np.exp(-margin), axis=1)
    return norm / norm.sum()

def savage(y, margin):
    e_2m = np.exp(margin)**2
    grad = -y * e_2m / (1 + e_2m)**3
    norm = np.linalg.norm(grad, axis=1)

    return norm / norm.sum()

def tangent(y, margin):
    grad = 4 * y * (2 * np.arctan(margin) - 1) / (1 + margin**2)
    norm = np.linalg.norm(grad, axis=1)

    return norm / norm.sum()


# Logic of Model
class _Node:
    def __init__(self, class_count, index):
        self.class_count = class_count
        self.index = index
        self.feature_id = 0
        self.threshold = 0.0
        self.is_leaf = True

    def _grow(self, X, y, w, feature_ids, classes):
        # 木の末端なら分裂する
        if self.is_leaf:
            # このノードに分類されたデータの数が一つ、クラスが一つもしくはデータの種類が1つのみの場合に分裂を中止する。
            X_ = X[np.ix_(self.index, feature_ids)]
            y_ = y[self.index]

            if self.index.sum()==1:
                return
            if (y_==y_[0]).all():
                return
            if (X_==X_[0]).all():
                return

            self.feature_id, self.threshold = _split(X, y, w, self.index, feature_ids, classes)
            count_left, index_left, count_right, index_right = to_left_and_right(X, y, self.index, classes, self.feature_id, self.threshold)

            self.left = _Node(count_left, index_left)
            self.right = _Node(count_right, index_right)
            self.is_leaf = False

        else:
            self.left._grow(X, y, w, feature_ids, classes)
            self.right._grow(X, y, w, feature_ids, classes)

class DecisionTree:
    def __init__(self, index, feature_ids):
        self._classes = None
        self.index = index
        self.feature_ids = feature_ids
        self.top = None

    def predict(self, X):
        prob = self.predict_prob(X)
        return self._classes[np.argmax(prob, axis=1)]

    def predict_prob(self, X):
        return np.array([self._predict_one(x) for x in X])

    def _predict_one(self, xs):
        node = self.top
        while not node.is_leaf:
            is_left = xs[node.feature_id] < node.threshold

            node = node.left if is_left else node.right

        return node.class_count / node.class_count.sum()

    def grow(self, X, y, w):
        if self.index is None: return

        y_ = y[self.index]

        if self.top is None:
            self._classes, count = np.unique(y_, return_counts=True)
            self.top = _Node(count, self.index)

        self.top._grow(X, y, w, self.feature_ids, self._classes)

class AlternatingDecisionForests:
    def __init__(self, n_estimators, max_depth):
        self.T = n_estimators
        self.D = max_depth

    def _update_weight(self, X, y):
        preds = np.array([e.predict_prob(X) for e in self.estimators])
        preds = np.mean(preds, axis=0)

        one_hot_ = np.zeros((self.N, len(self._classes))) - 1
        index = (np.arange(self.N), y)
        one_hot_[index] = 1

        margin = one_hot_ * preds

        self._w = tangent(one_hot_, margin)

    def fit(self, X, y):
        self.N = X.shape[0]
        self.n_features = X.shape[1]
        self._w = np.ones(self.N) / self.N
        self._classes = np.unique(y)
        self.n_features_forest = int(np.floor(np.sqrt(self.n_features)))
        self.estimators = []

        #Bootstrap
        indexes, columns = _bootstrap_sample(self.N, self.T, self.n_features)

        for ind, col in zip(indexes, columns):
            self.estimators.append(DecisionTree(ind, col))

        #fitting
        for i in tqdm(range(self.D)):
            for e in self.estimators:
                e.grow(X, y, self._w)

            self._update_weight(X, y)

    def predict(self, X):
        probs = np.array([e.predict_prob(X) for e in self.estimators])
        prob = np.mean(probs, axis=0)

        y_pred = self._classes[np.argmax(prob, axis=1)]

        return y_pred

    def score(self, X, y):
        y_pred = self.predict(X)

        return (y==y_pred).mean()
